package com.interview.blind_leet_code;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TwoSum {

	public static void main(String[] args) {

		System.out.println(twoSum(new Integer[] {3,3}, 6));
	}

	public static List<Integer> twoSum(Integer[] arr, int target) {

		HashMap<Integer, Integer> previousMap = new HashMap<>();
		List<Integer> l=new ArrayList<>();
		for (int i = 0; i < arr.length; i++) {
			Integer check = target - arr[i];
			if (!previousMap.isEmpty()&&previousMap.get(check)!=null) {
			l.add(i);
			l.add(previousMap.get(check));
					}
			else {
				previousMap.put(arr[i], i);
			}
		
		}
		return l;
	}
 
}
